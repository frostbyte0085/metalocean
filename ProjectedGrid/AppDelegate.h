//
//  AppDelegate.h
//  ProjectedGrid
//
//  Created by Pantelis Lekakis on 15/07/2015.
//  Copyright (c) 2015 Pantelis Lekakis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

