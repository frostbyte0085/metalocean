//
// A projected grid ocean with FFT/Perlin noise generation in the compute shader
// 3 perlin octaves for now.
// This is not organised very well, but it's supposed to be a demo.
//
#import "GameViewController.h"

@import Metal;
@import simd;
@import QuartzCore.CAMetalLayer;

static const int kOceanTesselation = 200;
static const float kOceanAspectBias = 0.0;

static const float kNearClipPlane = 0.01f;
static const float kFarClipPlane = 10000.0f;
static const float kFOV = 45.0f;
static const float kTimeScale = 1.0f;

static const float kPixelsEqualsDegrees = 0.1f;

static const float kFFTSize = 128;

// Max API memory buffer size.
static const size_t MAX_BYTES_PER_FRAME = 2048 * 2048;

typedef struct
{
    vector_float4 position;
} SkyboxVertex;

typedef struct
{
    vector_float4 position;
} OceanVertex;

typedef struct
{
    vector_float4 position;
    vector_float4 texcoord;
} QuadVertex;

typedef struct
{
    float density;
    vector_float3 color1;
    vector_float3 color2;
} Fog;

typedef struct
{
    matrix_float4x4 skybox_world_matrix;
    matrix_float4x4 projection_matrix;
    matrix_float4x4 view_matrix;
    matrix_float4x4 view_grid_matrix;
    matrix_float4x4 proj_view_grid_matrix;
    matrix_float4x4 proj_view_matrix;
    matrix_float4x4 inv_proj_view_matrix;
    matrix_float4x4 inv_proj_view_grid_matrix;
    matrix_float4x4 inv_view_matrix;
    matrix_float4x4 inv_proj_matrix;
    vector_float2 clip_planes;
    vector_float4 window_params;
    vector_float3 camera_position;
    vector_float4 timing; // x: elapsed, y: total time, z: total time * 2, w: total time * 0.5
} PerFrame;

typedef struct
{
    matrix_float4x4 object2world_matrix;
    matrix_float4x4 world2object_matrix;
} PerObject;

@implementation GameViewController
{
    // layer
    CAMetalLayer *_metalLayer;
    BOOL _layerSizeDidUpdate;
    MTLRenderPassDescriptor *_renderPassDescriptor;
    
    // controller
    CADisplayLink *_timer;
    BOOL _gameLoopPaused;
    id <MTLBuffer> _oceanUniformBuffer;
    id <MTLBuffer> _frameUniformBuffer;
    id <MTLBuffer> _fogUniformBuffer;
    
    // timing
    CFAbsoluteTime lastFrameTime;
    CFTimeInterval elapsedTime;
    CFTimeInterval currentTime;
    
    // renderer
    id <MTLCommandBuffer> _cmdBuffer;
    id <MTLDevice> _device;
    id <MTLCommandQueue> _commandQueue;
    id <MTLLibrary> _defaultLibrary;
    id <MTLTexture> _depthTex;
    id <MTLTexture> _msaaTex;
    
    // textures
    id<MTLTexture> _skyboxTexture;
    id<MTLSamplerState> _skyboxSampler;
    
    // ocean
    id <MTLRenderPipelineState> _oceanPipelineState;
    id <MTLDepthStencilState> _oceanDepthState;
    id <MTLBuffer> _vertexBufferOcean;
    id <MTLBuffer> _indexBufferOcean;
    id <MTLTexture> _perlinNoiseTexture;
    id <MTLTexture> _smallNormalsTexture;
    id <MTLTexture> _foamTexture;
    id <MTLTexture> _foamRamp;
    
    // skybox
    id <MTLRenderPipelineState> _skyboxPipelineState;
    id <MTLDepthStencilState> _skyboxDepthState;
    id <MTLBuffer> _vertexBufferSkybox;
    id <MTLBuffer> _indexBufferSkybox;
    
    // debug output
    id <MTLDepthStencilState> _debugDepthState;
    id <MTLRenderPipelineState> _debugPipelineState;
    id <MTLBuffer> _vertexBufferDebug1;
    
    // fft generation
    id <MTLComputePipelineState> _fftPipelineState;
    id <MTLTexture> _fftTexture;
    
    // uniforms
    PerFrame frameUniforms;
    PerObject oceanUniforms;
    Fog fogUniforms;
    
    // camera control
    vector_float2 previousTouchLocation;
    vector_float2 currentTouchLocation;
    vector_float2 deltaTouchLocation;
    float cameraAngleX;
    float cameraAngleZ;
}

- (void)dealloc
{
    [_timer invalidate];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _setupMetal];
    [self _loadAssets];
    
    _timer = [CADisplayLink displayLinkWithTarget:self selector:@selector(_gameloop)];
    [_timer addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (uint8_t *)dataForImage:(UIImage *)image
{
    CGImageRef imageRef = [image CGImage];
    
    // Create a suitable bitmap context for extracting the bits of the image
    const NSUInteger width = CGImageGetWidth(imageRef);
    const NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    uint8_t *rawData = (uint8_t *)calloc(height * width * 4, sizeof(uint8_t));
    const NSUInteger bytesPerPixel = 4;
    const NSUInteger bytesPerRow = bytesPerPixel * width;
    const NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    return rawData;
}

- (void)generateMipmapsForTexture:(id<MTLTexture>)texture
{
    if (texture != nil)
    {
        id <MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
        id<MTLBlitCommandEncoder> commandEncoder = [commandBuffer blitCommandEncoder];
        [commandEncoder generateMipmapsForTexture:texture];
        [commandEncoder endEncoding];
        [commandBuffer commit];
    }
}

- (id<MTLTexture>)texture2DWithImageNamed:(NSString *)imageName andMipmaps:(BOOL)mipmaps andFormat:(MTLPixelFormat)format
{
    UIImage *image = [UIImage imageNamed:imageName];
    CGSize imageSize = CGSizeMake(image.size.width * image.scale, image.size.height * image.scale);
    const NSUInteger bytesPerPixel = 4;
    const NSUInteger bytesPerRow = bytesPerPixel * imageSize.width;
    uint8_t *imageData = [self dataForImage:image];
    
    MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:format
                                                                                                 width:imageSize.width
                                                                                                height:imageSize.height
                                                                                             mipmapped:mipmaps];
    id<MTLTexture> texture = [_device newTextureWithDescriptor:textureDescriptor];
    
    MTLRegion region = MTLRegionMake2D(0, 0, imageSize.width, imageSize.height);
    [texture replaceRegion:region mipmapLevel:0 withBytes:imageData bytesPerRow:bytesPerRow];
    
    free(imageData);
    
    if (mipmaps)
    {
        [self generateMipmapsForTexture:texture];
    }
    
    return texture;
}

- (id<MTLTexture>)textureCubeWithImagesNamed:(NSArray *)imageNameArray andMipmaps:(BOOL)mipmaps andFormat:(MTLPixelFormat)format
{
    NSAssert(imageNameArray.count == 6, @"Cube texture can only be created from exactly six images");
    
    UIImage *firstImage = [UIImage imageNamed:[imageNameArray firstObject]];
    const CGFloat cubeSize = firstImage.size.width * firstImage.scale;
    
    const NSUInteger bytesPerPixel = 4;
    const NSUInteger bytesPerRow = bytesPerPixel * cubeSize;
    const NSUInteger bytesPerImage = bytesPerRow * cubeSize;
    
    MTLRegion region = MTLRegionMake2D(0, 0, cubeSize, cubeSize);
    
    MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor textureCubeDescriptorWithPixelFormat:format
                                                                                                    size:cubeSize
                                                                                               mipmapped:mipmaps];
    
    id<MTLTexture> texture = [_device newTextureWithDescriptor:textureDescriptor];
    
    for (size_t slice = 0; slice < 6; ++slice)
    {
        NSString *imageName = imageNameArray[slice];
        UIImage *image = [UIImage imageNamed:imageName];
        uint8_t *imageData = [self dataForImage:image];
        
        NSAssert(image.size.width == cubeSize && image.size.height == cubeSize, @"Cube map images must be square and uniformly-sized");
        
        [texture replaceRegion:region
                   mipmapLevel:0
                         slice:slice
                     withBytes:imageData
                   bytesPerRow:bytesPerRow
                 bytesPerImage:bytesPerImage];
        free(imageData);
    }
    
    if (mipmaps)
    {
        [self generateMipmapsForTexture:texture];
    }
    
    return texture;
}

- (void)_setupMetal
{
    // Find a usable device
    _device = MTLCreateSystemDefaultDevice();

    // Create a new command queue
    _commandQueue = [_device newCommandQueue];
    
    // Load all the shader files with a metal file extension in the project
    _defaultLibrary = [_device newDefaultLibrary];
    
    // Setup metal layer and add as sub layer to view
    _metalLayer = [CAMetalLayer layer];
    _metalLayer.device = _device;
    _metalLayer.pixelFormat = MTLPixelFormatBGRA8Unorm;
    
    // Change this to NO if the compute encoder is used as the last pass on the drawable texture
    _metalLayer.framebufferOnly = YES;
    
    // Add metal layer to the views layer hierarchy
    [_metalLayer setFrame:self.view.layer.frame];
    [self.view.layer addSublayer:_metalLayer];
    
    self.view.opaque = YES;
    self.view.backgroundColor = nil;
    self.view.contentScaleFactor = [UIScreen mainScreen].scale;
    
    
    lastFrameTime = CFAbsoluteTimeGetCurrent();
}

- (void) setupSkybox
{
    // make the depth stencil state
    MTLDepthStencilDescriptor *depthStateDesc = [[MTLDepthStencilDescriptor alloc] init];
    depthStateDesc.depthCompareFunction = MTLCompareFunctionLess;
    depthStateDesc.depthWriteEnabled = NO;
    _skyboxDepthState = [_device newDepthStencilStateWithDescriptor:depthStateDesc];
    
    // Create a pipeline state for the debug texture windows
    MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
    pipelineStateDescriptor.label = @"SkyboxPipeline";
    
    MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
    vertexDescriptor.attributes[0].format = MTLVertexFormatFloat4;
    vertexDescriptor.attributes[0].bufferIndex = 0;
    vertexDescriptor.attributes[0].offset = offsetof(SkyboxVertex, position);
    
    vertexDescriptor.layouts[0].stride = sizeof(SkyboxVertex);
    vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
    
    [pipelineStateDescriptor setSampleCount: 1];
    [pipelineStateDescriptor setVertexFunction:[_defaultLibrary newFunctionWithName:@"skybox_vertex"]];
    [pipelineStateDescriptor setFragmentFunction:[_defaultLibrary newFunctionWithName:@"skybox_fragment"]];
    pipelineStateDescriptor.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm;
    pipelineStateDescriptor.vertexDescriptor = vertexDescriptor;
    pipelineStateDescriptor.depthAttachmentPixelFormat = MTLPixelFormatDepth32Float;
    
    NSError* error = NULL;
    _skyboxPipelineState = [_device newRenderPipelineStateWithDescriptor:pipelineStateDescriptor error:&error];
    if (!_skyboxPipelineState) {
        NSLog(@"Failed to create debug pipeline state, error %@", error);
    }
    
    static float vertices[] =
    {
        // + Y
        -0.5,  0.5,  0.5, 1.0,
        0.5,  0.5,  0.5, 1.0,
        0.5,  0.5, -0.5, 1.0,
        -0.5,  0.5, -0.5, 1.0,
        // -Y
        -0.5, -0.5, -0.5, 1.0,
        0.5, -0.5, -0.5, 1.0,
        0.5, -0.5,  0.5, 1.0,
        -0.5, -0.5,  0.5, 1.0,
        // +Z
        -0.5, -0.5,  0.5, 1.0,
        0.5, -0.5,  0.5, 1.0,
        0.5,  0.5,  0.5, 1.0,
        -0.5,  0.5,  0.5, 1.0,
        // -Z
        0.5, -0.5, -0.5, 1.0,
        -0.5, -0.5, -0.5, 1.0,
        -0.5,  0.5, -0.5, 1.0,
        0.5,  0.5, -0.5, 1.0,
        // -X
        -0.5, -0.5, -0.5, 1.0,
        -0.5, -0.5,  0.5, 1.0,
        -0.5,  0.5,  0.5, 1.0,
        -0.5,  0.5, -0.5, 1.0,
        // +X
        0.5, -0.5,  0.5, 1.0,
        0.5, -0.5, -0.5, 1.0,
        0.5,  0.5, -0.5, 1.0,
        0.5,  0.5,  0.5, 1.0
    };
    
    static uint16_t indices[] =
    {
        0,  3,  2,  2,  1,  0,
        4,  7,  6,  6,  5,  4,
        8, 11, 10, 10,  9,  8,
        12, 15, 14, 14, 13, 12,
        16, 19, 18, 18, 17, 16,
        20, 23, 22, 22, 21, 20,
    };
    
    _vertexBufferSkybox = [_device newBufferWithBytes:vertices length:24 * sizeof(SkyboxVertex) options:0];
    _indexBufferSkybox = [_device newBufferWithBytes:indices length:36 * sizeof(unsigned short) options:0];
}

- (void) setupDebug
{
    // make the depth stencil state
    MTLDepthStencilDescriptor *depthStateDesc = [[MTLDepthStencilDescriptor alloc] init];
    depthStateDesc.depthCompareFunction = MTLCompareFunctionAlways;
    depthStateDesc.depthWriteEnabled = NO;
    _debugDepthState = [_device newDepthStencilStateWithDescriptor:depthStateDesc];
    
    // Create a pipeline state for the debug texture windows
    MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
    pipelineStateDescriptor.label = @"DebugPipeline";
    
    MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
    vertexDescriptor.attributes[0].format = MTLVertexFormatFloat4;
    vertexDescriptor.attributes[0].bufferIndex = 0;
    vertexDescriptor.attributes[0].offset = offsetof(QuadVertex, position);
    vertexDescriptor.attributes[1].format = MTLVertexFormatFloat4;
    vertexDescriptor.attributes[1].bufferIndex = 0;
    vertexDescriptor.attributes[1].offset = offsetof(QuadVertex, texcoord);
    
    vertexDescriptor.layouts[0].stride = sizeof(QuadVertex);
    vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
    
    [pipelineStateDescriptor setSampleCount: 1];
    [pipelineStateDescriptor setVertexFunction:[_defaultLibrary newFunctionWithName:@"quad_vertex"]];
    [pipelineStateDescriptor setFragmentFunction:[_defaultLibrary newFunctionWithName:@"quad_fragment"]];
    pipelineStateDescriptor.vertexDescriptor = vertexDescriptor;
    pipelineStateDescriptor.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm;
    pipelineStateDescriptor.depthAttachmentPixelFormat = MTLPixelFormatDepth32Float;
    
    NSError* error = NULL;
    _debugPipelineState = [_device newRenderPipelineStateWithDescriptor:pipelineStateDescriptor error:&error];
    if (!_debugPipelineState) {
        NSLog(@"Failed to create debug pipeline state, error %@", error);
    }
    
    QuadVertex* debug1 = make_quad(0, 0, 128, 128);
    _vertexBufferDebug1 = [_device newBufferWithBytes:debug1 length:sizeof(QuadVertex)*4 options:MTLResourceOptionCPUCacheModeDefault];
    free(debug1);
}

- (void)generateScreenSpaceGrid
{
    int width = (int)( (float)kOceanTesselation * (1.0 - kOceanAspectBias) - 0.5f);
    int height = (int)( (float)kOceanTesselation * (1.0 + kOceanAspectBias) + 0.5f);
    int size = width * height;
    int numIndices = ((width-1) * (height-1) * 2) * 3;
    
    OceanVertex* vertices = (OceanVertex*)malloc(size * sizeof(OceanVertex));
    unsigned int* indices = (unsigned int*)malloc(numIndices * sizeof(unsigned int));

    for (int y=0; y<height; y++) {
        float yy = (float)y / (float)(height-1);
        for (int x=0; x<width; x++) {
            
            float xx = (float)x / (float)(width-1);
            int idx = y * width + x;
            
            vertices[idx].position = vec4(xx*2-1, yy*2-1, 0, 1);
        }
    }

    int num = 0;
    int quadsX = width - 1;
    int quadsY = height - 1;

    for(int y=0; y<quadsY; y++) {
        for(int x=0; x<quadsX; x++) {
            
            int row0 = y * width;
            int row1 = (y+1) * width;
            
            indices[num++] = (row0 + x);
            indices[num++] = (row1 + x);
            indices[num++] = (row0 + x + 1);
            
            indices[num++] = (row1 + x);
            indices[num++] = (row1 + x + 1);
            indices[num++] = (row0 + x + 1);
        }
    }
     
    // Setup the vertex buffers
    _vertexBufferOcean = [_device newBufferWithBytes:vertices length:(sizeof(OceanVertex) * size) options:MTLResourceOptionCPUCacheModeDefault];
    _indexBufferOcean = [_device newBufferWithBytes:indices length:(sizeof(unsigned int) * numIndices) options:MTLResourceOptionCPUCacheModeDefault];
    
    free(indices);
    free(vertices);
    
    MTLDepthStencilDescriptor *depthStateDesc = [[MTLDepthStencilDescriptor alloc] init];
    depthStateDesc.depthCompareFunction = MTLCompareFunctionLess;
    depthStateDesc.depthWriteEnabled = YES;
    _oceanDepthState = [_device newDepthStencilStateWithDescriptor:depthStateDesc];
    
    // Create a reusable pipeline state
    MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
    pipelineStateDescriptor.label = @"OceanPipeline";

    MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
    vertexDescriptor.attributes[0].format = MTLVertexFormatFloat4;
    vertexDescriptor.attributes[0].bufferIndex = 0;
    vertexDescriptor.attributes[0].offset = offsetof(OceanVertex, position);
    
    vertexDescriptor.layouts[0].stride = sizeof(OceanVertex);
    vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
    
    [pipelineStateDescriptor setSampleCount: 1];
    [pipelineStateDescriptor setVertexFunction:[_defaultLibrary newFunctionWithName:@"ocean_vertex"]];
    [pipelineStateDescriptor setFragmentFunction:[_defaultLibrary newFunctionWithName:@"ocean_fragment"]];
    pipelineStateDescriptor.vertexDescriptor = vertexDescriptor;
    pipelineStateDescriptor.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm;
    pipelineStateDescriptor.depthAttachmentPixelFormat = MTLPixelFormatDepth32Float;
    
    NSError* error = NULL;
    _oceanPipelineState = [_device newRenderPipelineStateWithDescriptor:pipelineStateDescriptor error:&error];
    if (!_oceanPipelineState) {
        NSLog(@"Failed to created pipeline state, error %@", error);
    }
    
    _perlinNoiseTexture = [self texture2DWithImageNamed:@"perlin" andMipmaps:YES andFormat:MTLPixelFormatRGBA8Unorm];
    _smallNormalsTexture = [self texture2DWithImageNamed:@"normal" andMipmaps:YES andFormat:MTLPixelFormatRGBA8Unorm];
    _foamTexture = [self texture2DWithImageNamed:@"foam" andMipmaps:YES andFormat:MTLPixelFormatRGBA8Unorm];
    _foamRamp = [self texture2DWithImageNamed:@"foam_ramp" andMipmaps:YES andFormat:MTLPixelFormatRGBA8Unorm];
}

- (id<MTLSamplerState>) _makeSamplerStateWithMinFilter:(MTLSamplerMinMagFilter)minFilter andMagFilter:(MTLSamplerMinMagFilter)magFilter andMipFilter:(MTLSamplerMipFilter)mipFilter
{
    MTLSamplerDescriptor *samplerDescriptor = [MTLSamplerDescriptor new];
    samplerDescriptor.minFilter = minFilter;
    samplerDescriptor.magFilter = magFilter;
    samplerDescriptor.mipFilter = mipFilter;
    return [_device newSamplerStateWithDescriptor:samplerDescriptor];
}

- (void)setupComputeTargets
{
    NSError* error = nil;
    
    id <MTLFunction> fftKernel = [_defaultLibrary newFunctionWithName:@"process_texture"];
    _fftPipelineState = [_device newComputePipelineStateWithFunction:fftKernel error:&error];
    
    MTLTextureDescriptor* fftTextureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatRGBA8Unorm width:kFFTSize height:kFFTSize mipmapped:NO];
    _fftTexture = [_device newTextureWithDescriptor:fftTextureDescriptor];
}

- (void)_loadAssets
{
    // Allocate one region of memory for the ocean uniform buffer
    _oceanUniformBuffer = [_device newBufferWithLength:MAX_BYTES_PER_FRAME options:0];
    _oceanUniformBuffer.label = @"OceanBuffer";
    
    // and another one for the per-frame uniforms
    _frameUniformBuffer = [_device newBufferWithLength:MAX_BYTES_PER_FRAME options:0];
    _frameUniformBuffer.label = @"PerFrameBuffer";
    
    // and another for the fog and atmospherics
    _fogUniformBuffer = [_device newBufferWithLength:MAX_BYTES_PER_FRAME options:0];
    _fogUniformBuffer.label = @"FogBuffer";
    
    [self setupComputeTargets];
    [self setupSkybox];
    [self generateScreenSpaceGrid];
    [self setupDebug];
    
    NSArray *skyboxNames = @[@"SunSetLeft2048", @"SunSetRight2048", @"SunSetUp2048", @"SunSetDown2048", @"SunSetFront2048", @"SunSetBack2048"];
    
    _skyboxTexture = [self textureCubeWithImagesNamed:skyboxNames andMipmaps:YES andFormat:MTLPixelFormatRGBA8Unorm];
    _skyboxSampler = [self _makeSamplerStateWithMinFilter:MTLSamplerMinMagFilterLinear andMagFilter:MTLSamplerMinMagFilterLinear andMipFilter:MTLSamplerMipFilterLinear];
}

- (void)setupRenderPassDescriptorForTexture:(id <MTLTexture>) texture
{
    if (_renderPassDescriptor == nil)
        _renderPassDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
    
    _renderPassDescriptor.colorAttachments[0].texture = texture;
    _renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
    _renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.0f, 0.0f, 0.0f, 1.0f);
    _renderPassDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
    
    if (!_depthTex || (_depthTex && (_depthTex.width != texture.width || _depthTex.height != texture.height)))
    {
        //  If we need a depth texture and don't have one, or if the depth texture we have is the wrong size
        //  Then allocate one of the proper size
        
        MTLTextureDescriptor* desc = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat: MTLPixelFormatDepth32Float width: texture.width height: texture.height mipmapped: NO];
        _depthTex = [_device newTextureWithDescriptor: desc];
        _depthTex.label = @"Depth";
        
        _renderPassDescriptor.depthAttachment.texture = _depthTex;
        _renderPassDescriptor.depthAttachment.loadAction = MTLLoadActionClear;
        _renderPassDescriptor.depthAttachment.clearDepth = 1.0f;
        _renderPassDescriptor.depthAttachment.storeAction = MTLStoreActionDontCare;
    }
}

- (void)_renderSkyboxWithCommandEncoder:(id<MTLRenderCommandEncoder>)renderEncoder
{
    [renderEncoder setDepthStencilState:_skyboxDepthState];
    
    // Set context state
    [renderEncoder pushDebugGroup:@"DrawSkybox"];
    [renderEncoder setRenderPipelineState:_skyboxPipelineState];
    [renderEncoder setVertexBuffer:_vertexBufferSkybox offset:0 atIndex:0 ];
    [renderEncoder setVertexBuffer:_frameUniformBuffer offset:(sizeof(frameUniforms)) atIndex:1 ];
    [renderEncoder setFragmentTexture:_skyboxTexture atIndex:0];
    [renderEncoder setFragmentSamplerState:_skyboxSampler atIndex:0];
    
    // Tell the render context we want to draw our primitives
    [renderEncoder drawIndexedPrimitives:MTLPrimitiveTypeTriangle
                              indexCount:[_indexBufferSkybox length] / sizeof(unsigned short)
                               indexType:MTLIndexTypeUInt16
                             indexBuffer:_indexBufferSkybox
                       indexBufferOffset:0];
    
    [renderEncoder popDebugGroup];
}

- (void)_renderOceanWithCommandEncoder:(id<MTLRenderCommandEncoder>)renderEncoder
{
    [renderEncoder setDepthStencilState:_oceanDepthState];
    
    // Set context state
    [renderEncoder pushDebugGroup:@"DrawOcean"];
    [renderEncoder setRenderPipelineState:_oceanPipelineState];
    [renderEncoder setVertexBuffer:_vertexBufferOcean offset:0 atIndex:0 ];
    [renderEncoder setVertexBuffer:_oceanUniformBuffer offset:(sizeof(oceanUniforms)) atIndex:1 ];
    [renderEncoder setVertexBuffer:_frameUniformBuffer offset:(sizeof(frameUniforms)) atIndex:2 ];
    [renderEncoder setFragmentBuffer:_fogUniformBuffer offset:sizeof(fogUniforms) atIndex:0];
    [renderEncoder setVertexTexture:_perlinNoiseTexture atIndex:0];
    [renderEncoder setFragmentTexture:_perlinNoiseTexture atIndex:0];
    [renderEncoder setFragmentTexture:_smallNormalsTexture atIndex:1];
    [renderEncoder setFragmentTexture:_skyboxTexture atIndex:2];
    [renderEncoder setFragmentTexture:_foamTexture atIndex:3];
    [renderEncoder setFragmentTexture:_foamRamp atIndex:4];
    
    [renderEncoder drawIndexedPrimitives:MTLPrimitiveTypeTriangle
                              indexCount:[_indexBufferOcean length] / sizeof(unsigned int)
                               indexType:MTLIndexTypeUInt32
                             indexBuffer:_indexBufferOcean
                       indexBufferOffset:0];
    
    [renderEncoder popDebugGroup];
}

- (void) _renderDebugWithCommandEncoder:(id<MTLRenderCommandEncoder>)renderEncoder
{
    [renderEncoder setDepthStencilState:_debugDepthState];
    
    // Set context state
    [renderEncoder pushDebugGroup:@"DebugQuads"];
    [renderEncoder setRenderPipelineState:_debugPipelineState];
    [renderEncoder setVertexBuffer:_frameUniformBuffer offset:(sizeof(frameUniforms)) atIndex:1 ];
    
    [renderEncoder setVertexBuffer:_vertexBufferDebug1 offset:0 atIndex:0 ];
    [renderEncoder setFragmentTexture:_fftTexture atIndex:0];
    
    // Tell the render context we want to draw our primitives
    [renderEncoder drawPrimitives:MTLPrimitiveTypeTriangleStrip vertexStart:0 vertexCount:4];
}

- (void)_compute
{
    MTLSize threadGroupCount = MTLSizeMake(8, 8, 1);
    MTLSize threadGroups = MTLSizeMake(kFFTSize/threadGroupCount.width, kFFTSize/threadGroupCount.height, 1);
    
    // create a command buffer to be used for the compute shader
    id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
    commandBuffer.label = @"ComputeCommand";
    
    // and a compute encoder
    id<MTLComputeCommandEncoder> commandEncoder = [commandBuffer computeCommandEncoder];
    
    [commandEncoder setComputePipelineState:_fftPipelineState];
    [commandEncoder setTexture:_fftTexture atIndex:0];
    [commandEncoder dispatchThreadgroups:threadGroupCount threadsPerThreadgroup:threadGroups];
    
    [commandEncoder endEncoding];
    [commandBuffer commit];
}

- (void)_render
{
    // Create a new command buffer for each renderpass to the current drawable
    id <MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
    commandBuffer.label = @"RenderCommand";
    
    // obtain a drawable texture for this render pass and set up the renderpass descriptor for the command encoder to render into
    id <CAMetalDrawable> drawable = [_metalLayer nextDrawable];
    [self setupRenderPassDescriptorForTexture:drawable.texture];
    
    // Create a render command encoder so we can render into something
    id <MTLRenderCommandEncoder> renderEncoder = [commandBuffer renderCommandEncoderWithDescriptor:_renderPassDescriptor];
    renderEncoder.label = @"RenderEncoder";
//    [renderEncoder setCullMode:MTLCullModeBack];
    
    [self _renderSkyboxWithCommandEncoder:renderEncoder];
    [self _renderOceanWithCommandEncoder:renderEncoder];
    //[self _renderDebugWithCommandEncoder:renderEncoder];
    
    [renderEncoder endEncoding];
    
    // Schedule a present once the framebuffer is complete
    [commandBuffer presentDrawable:drawable];
    
    // Finalize rendering here & push the command buffer to the GPU
    [commandBuffer commit];
}

- (void)_reshape
{

}

- (void)_update
{
    currentTime += elapsedTime * kTimeScale;
    deltaTouchLocation *= (1.0 - elapsedTime * 10);
    
    // Per frame
    float aspect = fabs(self.view.bounds.size.width / self.view.bounds.size.height);
    
    // camera control
    //cameraAngleX += deltaTouchLocation.y * kPixelsEqualsDegrees;
    cameraAngleZ += deltaTouchLocation.x * kPixelsEqualsDegrees;
    
    frameUniforms.camera_position = vec3(0,2.5,0);
    
    matrix_float4x4 cameraRotation = matrix_multiply(matrix_from_rotation( (M_PI / 180.0f) * cameraAngleZ, 0.0f, 1.0f, 0.0f), matrix_from_rotation((M_PI / 180.0f) * cameraAngleX, 1.0f, 0.0f, 0.0f));
    matrix_float4x4 cameraTranslation = matrix_from_translation(frameUniforms.camera_position);
    matrix_float4x4 cameraWorldMatrix = matrix_multiply(cameraRotation, cameraTranslation);
    
    matrix_float4x4 gridCameraWorldMatrix = matrix_multiply(cameraRotation, matrix_from_translation(frameUniforms.camera_position - vec3(0,0,0.007)));
    
    frameUniforms.skybox_world_matrix = matrix_from_translation(frameUniforms.camera_position);
    frameUniforms.projection_matrix = matrix_from_perspective_fov_aspectLH(kFOV * (M_PI / 180.0f), aspect, kNearClipPlane, kFarClipPlane);
    frameUniforms.view_matrix = matrix_invert(cameraWorldMatrix);
    frameUniforms.view_grid_matrix = matrix_invert(gridCameraWorldMatrix);
    frameUniforms.proj_view_matrix = matrix_multiply(frameUniforms.projection_matrix, frameUniforms.view_matrix);
    frameUniforms.proj_view_grid_matrix = matrix_multiply(frameUniforms.projection_matrix, frameUniforms.view_grid_matrix);
    frameUniforms.inv_view_matrix = matrix_invert(frameUniforms.view_matrix);
    frameUniforms.inv_proj_matrix = matrix_invert(frameUniforms.projection_matrix);
    frameUniforms.inv_proj_view_matrix = matrix_invert(frameUniforms.proj_view_grid_matrix);
    frameUniforms.inv_proj_view_grid_matrix = matrix_invert(frameUniforms.proj_view_grid_matrix);
    frameUniforms.clip_planes = vec2(kNearClipPlane, kFarClipPlane);
    frameUniforms.timing = vec4(elapsedTime, currentTime, currentTime * 2.0f, currentTime * 0.5f);
    frameUniforms.window_params = vec4(self.view.bounds.size.width, self.view.bounds.size.height, 1.0f / self.view.bounds.size.width, 1.0f / self.view.bounds.size.height);
    
    
    uint8_t *frameBufferPointer = (uint8_t*)[_frameUniformBuffer contents] + (sizeof(frameUniforms));
    memcpy(frameBufferPointer, &frameUniforms, sizeof(frameUniforms));
    
    // ocean uniforms
    oceanUniforms.object2world_matrix = matrix_from_translation(vec3(0,0,0));
    oceanUniforms.world2object_matrix = matrix_invert(oceanUniforms.object2world_matrix);
    
    // Load constant buffer data into appropriate buffer at current index
    uint8_t *oceanBufferPointer = (uint8_t *)[_oceanUniformBuffer contents] + (sizeof(oceanUniforms));
    memcpy(oceanBufferPointer, &oceanUniforms, sizeof(oceanUniforms));
    
    // fog parameters
    fogUniforms.density = 0.005;
    fogUniforms.color1 = vec3(0.31,0.298,0.27);
    fogUniforms.color2 = vec3(0.99,0.8,0.4);
    
    uint8_t *fogBufferPointer = (uint8_t*)[_fogUniformBuffer contents] + (sizeof(fogUniforms));
    memcpy(fogBufferPointer, &fogUniforms, sizeof(fogUniforms));
}

// The main game loop called by the CADisplayLine timer
- (void)_gameloop
{
    @autoreleasepool {
        if (_layerSizeDidUpdate)
        {
            CGFloat nativeScale = self.view.window.screen.nativeScale;
            CGSize drawableSize = self.view.bounds.size;
            drawableSize.width *= nativeScale;
            drawableSize.height *= nativeScale;
            _metalLayer.drawableSize = drawableSize;
            
            [self _reshape];
            _layerSizeDidUpdate = NO;
        }
        
        CFAbsoluteTime frameTime = CFAbsoluteTimeGetCurrent();
        elapsedTime = (frameTime - lastFrameTime);
        lastFrameTime = frameTime;
        
        // update
        [self _update];
        
        // do compute stuff
        [self _compute];
        
        // draw
        [self _render];
    }
}

// Called whenever view changes orientation or layout is changed
- (void)viewDidLayoutSubviews
{
    _layerSizeDidUpdate = YES;
    [_metalLayer setFrame:self.view.layer.frame];
}

- (void) touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    CGPoint loc = [[touches anyObject] locationInView:self.view];
    previousTouchLocation = currentTouchLocation = vec2(loc.x, loc.y);
}

- (void) touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
    // Calculate offset
    CGPoint pt = [[touches anyObject] locationInView:self.view];
    previousTouchLocation = currentTouchLocation;
    currentTouchLocation = vec2(pt.x, pt.y);
    
    deltaTouchLocation = currentTouchLocation - previousTouchLocation;
}

#pragma mark Utilities

static matrix_float4x4 matrix_from_perspective_fov_aspectLH(const float fovY, const float aspect, const float nearZ, const float farZ)
{
    float yscale = 1.0f / tanf(fovY * 0.5f); // 1 / tan == cot
    float xscale = yscale / aspect;
    float q = farZ / (farZ - nearZ);
    
    matrix_float4x4 m = {
        .columns[0] = { xscale, 0.0f, 0.0f, 0.0f },
        .columns[1] = { 0.0f, yscale, 0.0f, 0.0f },
        .columns[2] = { 0.0f, 0.0f, q, 1.0f },
        .columns[3] = { 0.0f, 0.0f, q * -nearZ, 0.0f }
    };
    
    return m;
}

static matrix_float4x4 matrix_from_translation(vector_float3 v)
{
    matrix_float4x4 m = matrix_identity_float4x4;
    m.columns[3] = (vector_float4) { v.x, v.y, v.z, 1.0 };
    return m;
}

static matrix_float4x4 matrix_from_rotation(float radians, float x, float y, float z)
{
    vector_float3 v = vector_normalize(((vector_float3){x, y, z}));
    float cos = cosf(radians);
    float cosp = 1.0f - cos;
    float sin = sinf(radians);
    
    matrix_float4x4 m = {
        .columns[0] = {
            cos + cosp * v.x * v.x,
            cosp * v.x * v.y + v.z * sin,
            cosp * v.x * v.z - v.y * sin,
            0.0f,
        },
        
        .columns[1] = {
            cosp * v.x * v.y - v.z * sin,
            cos + cosp * v.y * v.y,
            cosp * v.y * v.z + v.x * sin,
            0.0f,
        },
        
        .columns[2] = {
            cosp * v.x * v.z + v.y * sin,
            cosp * v.y * v.z - v.x * sin,
            cos + cosp * v.z * v.z,
            0.0f,
        },
        
        .columns[3] = { 0.0f, 0.0f, 0.0f, 1.0f
        }
    };
    return m;
}

static matrix_float4x4 matrix_from_quat(vector_float4 q)
{
    q = vector_normalize(q);
    
    float qx = q.x;
    float qy = q.y;
    float qz = q.z;
    float qw = q.w;
    
    matrix_float4x4 m = {
        .columns[0] = {1.0f - 2.0f*qy*qy - 2.0f*qz*qz, 2.0f*qx*qy - 2.0f*qz*qw, 2.0f*qx*qz + 2.0f*qy*qw, 0.0f,},
        
        .columns[1] = {
            2.0f*qx*qy + 2.0f*qz*qw, 1.0f - 2.0f*qx*qx - 2.0f*qz*qz, 2.0f*qy*qz - 2.0f*qx*qw, 0.0f,},
        
        .columns[2] = {
            2.0f*qx*qz - 2.0f*qy*qw, 2.0f*qy*qz + 2.0f*qx*qw, 1.0f - 2.0f*qx*qx - 2.0f*qy*qy, 0.0f,},
        
        .columns[3] = { 0.0f, 0.0f, 0.0f, 1.0f}
    };
    return m;
}

static vector_float4 vec4(float x, float y, float z, float w)
{
    vector_float4 v;
    
    v.x = x;
    v.y = y;
    v.z = z;
    v.w = w;
    
    return v;
}

static vector_float3 vec3(float x, float y, float z)
{
    vector_float3 v;
    
    v.x = x;
    v.y = y;
    v.z = z;
    
    return v;
}

static vector_float2 vec2(float x, float y)
{
    vector_float2 v;
    
    v.x = x;
    v.y = y;
    
    return v;
}

static vector_float4 quat_from_euler(float yaw, float pitch, float roll)
{
    vector_float4 q;
    
    yaw *= ((M_PI / 180.0f) * yaw);
    pitch *= ((M_PI / 180.0f) * pitch);
    roll *= ((M_PI / 180.0f) * roll);
    
    float num9 = roll * 0.5f;
    float num6 = sinf(num9);
    float num5 = cosf(num9);
    float num8 = pitch * 0.5f;
    float num4 = sinf(num8);
    float num3 = cosf(num8);
    float num7 = yaw * 0.5f;
    float num2 = sinf(num7);
    float num = cosf(num7);
    q.x = ((num * num4) * num5) + ((num2 * num3) * num6);
    q.y = ((num2 * num3) * num5) - ((num * num4) * num6);
    q.z = ((num * num3) * num6) - ((num2 * num4) * num5);
    q.w = ((num * num3) * num5) + ((num2 * num4) * num6);
    
    return q;
}

static QuadVertex* make_quad(float x, float y, float width, float height)
{
    QuadVertex* vertices = (QuadVertex*)malloc(sizeof(QuadVertex) * 4);
    
    vertices[0].position = vec4(x, y + height, 0, 1);
    vertices[1].position = vec4(x + width, y + height, 0, 1);
    vertices[2].position = vec4(x, y, 0, 1);
    vertices[3].position = vec4(x + width, y, 0, 1);
    
    return vertices;
}

@end
