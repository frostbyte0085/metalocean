//
//  Compute.metal
//  ProjectedGrid
//
//  Created by Pantelis Lekakis on 18/07/2015.
//  Copyright (c) 2015 Pantelis Lekakis. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

kernel void process_texture(texture2d<float, access::write> outTexture [[texture(0)]],
                            uint2 gid [[thread_position_in_grid]])
{
    float4 color = float4(1,0,0,1);
    
    outTexture.write(color, gid);
}
