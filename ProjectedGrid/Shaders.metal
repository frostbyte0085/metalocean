//
//  Shaders.metal
//  ProjectedGrid
//
//  Created by Pantelis Lekakis on 15/07/2015.
//  Copyright (c) 2015 Pantelis Lekakis. All rights reserved.
//

#include <metal_stdlib>
#include <simd/simd.h>

using namespace metal;

typedef struct
{
    float density;
    vector_float3 color1;
    vector_float3 color2;
} Fog;

typedef struct
{
    matrix_float4x4 object2world_matrix;
    matrix_float4x4 world2object_matrix;
    
} PerObject;

typedef struct
{
    matrix_float4x4 skybox_world_matrix;
    matrix_float4x4 projection_matrix;
    matrix_float4x4 view_matrix;
    matrix_float4x4 view_grid_matrix;
    matrix_float4x4 proj_view_grid_matrix;
    matrix_float4x4 proj_view_matrix;
    matrix_float4x4 inv_proj_view_matrix;
    matrix_float4x4 inv_proj_view_grid_matrix;
    matrix_float4x4 inv_view_matrix;
    matrix_float4x4 inv_proj_matrix;
    vector_float2 clip_planes;
    vector_float4 window_params;
    vector_float3 camera_position;
    vector_float4 timing; // x: elapsed, y: total time, z: total time * 2, w: total time * 0.5
} PerFrame;

typedef struct
{
    packed_float4 position;
} SkyboxVertex;

typedef struct
{
    packed_float4 position;
} OceanVertex;

typedef struct
{
    packed_float4 position;
    float4 texcoord;
} QuadVertex;

typedef struct
{
    float4 position [[position]];
    float4 worldPos;
    float depth;
    float4 viewDir;
    float3 lightDir;
    float2 perlinUV0;
    float2 perlinUV1;
    float2 perlinUV2;
    float4 smallUV;
    float3 tangent;
    float3 bitangent;
    float3 normal;
    float4 foamUV;
    
} V2F_Ocean;

typedef struct
{
    float4 position [[position]];
    float4 texcoord;
} V2F_Quad;

typedef struct
{
    float4 position [[position]];
    float4 texcoord;
} V2F_Skybox;

float3 applyFog(float3 sourceColor, float depth, float fcolor, constant Fog& fogUniforms)
{
    float exponent = depth * fogUniforms.density;
    exponent *= exponent;
    
    float f = 1.0 / exp(exponent);
    //return float3(mix( pow(fogUniforms.color, 2.2), sourceColor, f));
    return float3(mix( mix(fogUniforms.color1, fogUniforms.color2, fcolor), sourceColor, f));
}

/////////////////////////////////////////////////
//
// OCEAN RENDERING
//
/////////////////////////////////////////////////
float3 unproject(matrix_float4x4 invVP, float3 position)
{
    float4 p = invVP * float4(position, 1.0);
    return p.xyz / p.w;
}

float4 reconstruct(matrix_float4x4 invVP, float2 clipPlanes, float3 cameraPosition, float4 v) {
    float4 waterPlane = float4(0,1,0,0);
				
	float3  rayStart = unproject(invVP, float3(v.xy, 0));
    float3  rayDir   = normalize(rayStart - cameraPosition);
    float   nDotDir  = dot(rayDir, waterPlane.xyz);
    float   dist = 0.0f;
    
    float4 waterVertex = 0;
    if (nDotDir > -0.00001)
    {
        // projection back-firing or intersection is far beyond far plane, replace vertex with far plane intersection
        dist = clipPlanes.y;
        float3 rayEnd = unproject(invVP, float3(v.xy, 1.0));
        waterVertex.xyz = rayEnd - waterPlane.xyz * dot(waterPlane.xyz, rayEnd - cameraPosition);
    }
    else
    {
        // regular intersection, but it may still be beyond far plane, so limit maximum distance
        dist        = min((-dot(rayStart, waterPlane.xyz)) / nDotDir, clipPlanes.y);
        waterVertex.xyz = rayStart + dist * rayDir;
    }
    
    waterVertex.y = 0;
    waterVertex.w = dist;
    
    return waterVertex;
}

constexpr sampler s (coord::normalized, address::repeat, filter::linear, mip_filter::linear);
constexpr sampler foamRampSampler (coord::normalized, address::clamp_to_edge, filter::nearest);

// Ocean vs
vertex V2F_Ocean ocean_vertex(device OceanVertex* vertex_array [[ buffer(0) ]],
                                  constant PerObject& objectUniforms [[ buffer(1) ]],
                                  constant PerFrame& frameUniforms [[ buffer(2) ]],
                                  texture2d<float> heightmap [[texture(0)]],
                                  unsigned int vid [[ vertex_id ]])
{
    V2F_Ocean out;
    
    const float2 tileSize = float2(16, 16);
    const float3 perlinOctaves = float3(0.35, 0.25, 0.085);
    const float3 perlinAmplitudes = float3(0.45, 0.6, 1);
    const float2 smallUVTile = float2(2, 2);
    const float maxLOD = 10.0;
    const float lodExponent = 0.009;
    const float waveIntensity = 1.0;
    const float2 foamSize = float2(5, 5);
    
    float4 waterVertex = reconstruct(frameUniforms.inv_proj_view_grid_matrix, frameUniforms.clip_planes, frameUniforms.camera_position, vertex_array[vid].position);
    float viewSpaceDepth = length(frameUniforms.view_grid_matrix * float4(waterVertex.xyz, 1.0));
    
    float2 texcoord = waterVertex.xz / tileSize;
    float mipLevel = (1.0 - exp(-lodExponent * waterVertex.w)) * maxLOD;
    
    float2 waveMovement = float2(frameUniforms.timing.w * 0.05, 0);
    float3 amplitudes = mix(float3(0,0,0), perlinAmplitudes, waveIntensity);
    
    out.perlinUV0 = texcoord * perlinOctaves.x + waveMovement;
    out.perlinUV1 = texcoord * perlinOctaves.y + waveMovement;
    out.perlinUV2 = texcoord * perlinOctaves.z + waveMovement;
    
    out.smallUV.xy = texcoord * smallUVTile.x + waveMovement * smallUVTile.x;
    out.smallUV.zw = texcoord * smallUVTile.y - waveMovement * smallUVTile.y;
    
    float perlin0 = heightmap.sample(s, out.perlinUV0, level(mipLevel)).z;
    float perlin1 = heightmap.sample(s, out.perlinUV1, level(mipLevel)).z;
    float perlin2 = heightmap.sample(s, out.perlinUV2, level(mipLevel)).z;
    
    float totalPerlin = (perlin0 * amplitudes.x + perlin1 * amplitudes.y + perlin2 * amplitudes.z);
    
    waterVertex.y = (totalPerlin * 2 - 1);
    
    matrix_float4x4 mvp = frameUniforms.projection_matrix * objectUniforms.object2world_matrix * frameUniforms.view_grid_matrix;
    
    out.worldPos.xyz = waterVertex.xyz;
    out.worldPos.w = totalPerlin;
    
    out.position = mvp * float4(waterVertex.xyz, 1.0);
    out.depth = viewSpaceDepth;
    
    // tbn
    out.normal = float3(0,1,0);
    out.tangent = float3(1,0,0);
    out.bitangent = float3(0,0,1);
    
    out.viewDir.xyz = (waterVertex.xyz - frameUniforms.camera_position);
    out.viewDir.w = waveIntensity;
    out.lightDir = (float3(-1, -0.5, 0.45));
    out.foamUV.xy = waterVertex.xz / foamSize;
    
    return out;
}

float fresnel (float NdotV)
{
    return pow(saturate(1 - NdotV), 5);
}

// Ocean fs
fragment float4 ocean_fragment(V2F_Ocean in [[stage_in]],
                              constant Fog& fogUniforms [[buffer(0)]],
                               texture2d<float> heightmap[[texture(0)]],
                               texture2d<float> smallNormals[[texture(1)]],
                               texturecube<float> reflection [[texture(2)]],
                               texture2d<float> foam [[texture(3)]],
                               texture2d<float> foamRamp[[texture(4)]])
{
    const float3 normalGradient = float3(1.5,1.2,0.85);
    const float3 waterColor = float3(0.1, 0.1, 0.15);
    const float3 surfaceColor = float3(1,1,1);
    const float3 specularColor = float3(1, 1, 0.67);
    
    float4 color = 0;
    
    float3 V = normalize(in.viewDir.xyz);
    float3 L = normalize(in.lightDir);
    
    float2 normal0 = heightmap.sample(s, in.perlinUV0).xy*2-1;
    float2 normal1 = heightmap.sample(s, in.perlinUV1).xy*2-1;
    float2 normal2 = heightmap.sample(s, in.perlinUV2).xy*2-1;
    
    float3 smallNormal0 = smallNormals.sample(s, in.smallUV.xy).xyz*2-1;
    float3 smallNormal1 = smallNormals.sample(s, in.smallUV.zw).xyz*2-1;
    
    float2 totalNormal = (normal0 * normalGradient.x + normal1 * normalGradient.y + normal2 * normalGradient.z);
    
    float3 N = normalize(float3(totalNormal, in.worldPos.w) + smallNormal0 + smallNormal1);
    float3x3 tbn = float3x3(in.tangent, in.bitangent, in.normal);
    N = (tbn * N);
    N = mix(float3(0,1,0), N, in.viewDir.w);
    
    float3 refl = reflection.sample(s, reflect(V, N), level(in.viewDir.w * 6)).xyz;
    
    // foam accumulation
    float foamFactor = smoothstep(0.7, 2.5, in.worldPos.y);
    float3 foamRampValue = foamRamp.sample(foamRampSampler, float2(foamFactor, 0.5)).rgb;
    float3 foamColor = foam.sample(s, in.foamUV.xy).rgb;
    float foamAccumulation = saturate(dot(foamColor, foamRampValue));
    
    float NdotL = saturate(dot(N, L));
    float NdotV = saturate(dot(N, V));
    
    float f = fresnel(NdotV);
    
    float3 H = normalize(L + V);
    float specular = pow(dot(H, N), 60);
    
    color.rgb = mix(waterColor, refl * surfaceColor, f);
    color.rgb += foamAccumulation;
    color.rgb += specular * specularColor * (1-foamAccumulation);
    
    color.rgb =  applyFog(color.rgb, in.depth, pow(saturate(dot(V, -L)), 16.0)*1.5, fogUniforms);
    return color;
}
/////////////////////////////////////////////////

/////////////////////////////////////////////////
//
// QUAD RENDERING
//
/////////////////////////////////////////////////

// Quad vs
vertex V2F_Quad quad_vertex(device QuadVertex* vertex_array [[ buffer(0) ]],
                            constant PerFrame& frameUniforms [[buffer(1)]],
                              unsigned int vid [[ vertex_id ]])
{
    V2F_Quad out;
    
    float4 p = vertex_array[vid].position;
    p.xy /= frameUniforms.window_params.xy;
    p.y = 1.0 - p.y;
    
    p.xy = p.xy * 2.0 - 1.0;
    
    out.position = p;
    out.texcoord = vertex_array[vid].texcoord;
    
    return out;
}

// Quad fs
fragment float4 quad_fragment(V2F_Quad in [[stage_in]],
                             texture2d<float> tex [[texture(0)]],
                             sampler samp [[sampler(0)]])
{
    return tex.sample (samp, in.texcoord.xy);
}
/////////////////////////////////////////////////

/////////////////////////////////////////////////
//
// SKYBOX RENDERING
//
/////////////////////////////////////////////////

// Quad vs
vertex V2F_Skybox skybox_vertex(device SkyboxVertex* vertex_array [[ buffer(0) ]],
                            constant PerFrame& frameUniforms [[buffer(1)]],
                            unsigned int vid [[ vertex_id ]])
{
    V2F_Skybox out;
    
    float4 position = vertex_array[vid].position;
    
    matrix_float4x4 mvp = frameUniforms.projection_matrix * frameUniforms.skybox_world_matrix * frameUniforms.view_matrix;
    out.position = mvp * position;
    out.texcoord = normalize(position);
    
    return out;
}

// Quad fs
fragment half4 skybox_fragment(V2F_Skybox in [[stage_in]],
                             texturecube<half> tex [[texture(0)]],
                             sampler samp [[sampler(0)]])
{
    float3 tc = float3(in.texcoord.x, in.texcoord.y, in.texcoord.z);
    return tex.sample(samp, tc);
}

/////////////////////////////////////////////////
